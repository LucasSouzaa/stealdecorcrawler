/**
 * Created by Lucas Souza on 11/07/2018.
 */
const express = require('express');
const controller = require('../resources/product/user.controller');

const router = express.Router();

router.post('/product', controller.find);

module.exports = router;