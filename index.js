var express = require('express');
const routes = require('./http/routes')
var cors = require('cors');
var bodyParser = require('body-parser')


var app = express();


var whitelist = [
    'http://0.0.0.0:3000',
];
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', false);

    // Pass to next layer of middleware
    next();
});

app.use(bodyParser.json({limit: '7mb'}))
app.use(bodyParser.urlencoded({limit: '7mb', extended: true}))
app.use('/api/', routes);


var server = app.listen(process.env.PORT || 5000, function () {
    var port = server.address().port;
    console.log("Express is working on port " + port);
});

