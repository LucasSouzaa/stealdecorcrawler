/**
 * Created by Lucas Souza on 11/07/2018.
 */
const service = require('./user.service');

const find = async(req, res, next) =>
{
    res.setHeader('Access-Control-Allow-Origin', '*');
    try {
        let response = await service.getList(req.body);//qawait service.getList(since);
        res.send(response)
    } catch (e) {
        return res.send({error: e.message})
    }
}

const controller = {
    find,
};

module.exports = controller;