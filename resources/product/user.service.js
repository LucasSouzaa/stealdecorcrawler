/**
 * Created by lucs on 11/07/2018.
 */
const puppeteer = require('puppeteer');

const getList = async (product) => {
    try{
        var item = product;

        console.log(item.url);
        const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
        const page = await browser.newPage();
        await page.goto(item.url, {waitUntil: 'networkidle2'});


        try {

            const textContent = await page.evaluate((item) => {


                var name = document.querySelector(item.productNameSelector);
                var price = document.querySelector(item.productPriceSelector);
                var salePrice = document.querySelector(item.productSalePriceSelector);
                var image = document.querySelector(item.productImageSelector);

                // if(image !== null){
                //     if(image.style.backgroundImage === ""){
                //         image = image.src;
                //     }else{
                //         image = image.style.backgroundImage;
                //     }
                // }

                return {
                    url: item.url,
                    name: name !== null ? name.textContent : null,
                    imageurl: image !== null ? image.style.backgroundImage : null,
                    price: price !== null ? price.textContent : 0,
                    sale_price: salePrice !== null ? salePrice.textContent : 0,
                    store_id: item.id
                };

            }, item);

            await browser.close();
            console.log(textContent);
            return textContent;


        } catch (e) {
            return {"error": e};
        }

    }catch(e){
        return {"error": e};
    }
}

module.exports = {
    getList,
}
